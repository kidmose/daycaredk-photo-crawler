# daycare.dk image crawler

Crawls daycare.dk for photos

Status: working beta (Authing/cookies is a bit hacky, config is hardcoded)

I run with [nix](https://nixos.org/download.html) - `nix-shell --pure
--run 'python crawl.py'` - so if you don't, check `default.nix` for
environment/requirements.

Usage:

 1. Log in, get cookies for auth, and save them to `cookies.pkl`.
 2. Optional: symlink `data` to where you want photos to go.
 3. `python crawl.py`
