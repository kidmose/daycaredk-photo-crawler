#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from urllib.parse import urlparse
import requests
import os, time, re, pickle
from pathlib import Path
from datetime import datetime
from tempfile import TemporaryFile

def _wait_for_load():
    for _ in range(50):
        rs = driver.execute_script("return document.readyState")
        if rs in ["loaded", "complete"]:
            break
        time.sleep(.1)

# JavaScript lifted from page source, for getting image urls
_DOWNLOADIMAGE_JS = """
//    function downloadimage() {

        //window.open($find('controlsgalleryNew_RadRotator2').get_currentItem().get_element().getElementsByTagName('IMG')['ctlPhoto'].src);

        var currentItem = $find('controlsgalleryNew_RadRotator2').get_currentItem().get_element();
        var fullPath = currentItem.getElementsByTagName('div');
        var path = "";

        for (i = 0, j = fullPath.length; i < j; i++) {

            var test = fullPath[i].id.toString();

            if (test.indexOf("ctlDivFoto") != -1)
            {
                path = fullPath[i].style.backgroundImage;
            }

        }

        //var fileNoExt = file.replace(/\.(html|htm)$/, "");
        path = path.replace(/^url\([\"\']*|[\"\']*\)$/g, '') // trim url("...") or url(...) [in IE 8 case]
        //var path = $.url(path).attr("path");
return path;
"""

print("Start browser")
options = Options()
options.headless = False
driver = Firefox(options=options)

# Make sure to get cookies before taking control
# `driver.get` seems to break manual log-in

try:
    cookies = pickle.load(open("cookies.pkl", "rb"))
except Exception as e:
    print("Log in manually and follow up with:")
    print('pickle.dump(driver.get_cookies(), open("cookies.pkl", "wb"))')
    raise e

driver.get('https://home.daycare.dk/')
 
for cookie in cookies:
    driver.add_cookie(cookie)

driver.refresh()

print("Going to kid's page")
driver.find_element_by_name('ctlTopKid').click()

print("Going to 'Billeder'")
driver.find_element_by_id('ctlBillederList').click()

print("Going to first photo")
driver.switch_to.frame(driver.find_element_by_id('RadpaneMainContent'))

# Something throws us of here, maybe switching frames?
# So require a manual step;
def get_counting_element():
    try:
        return driver.find_element_by_id('controlsgalleryNew_lblCount')
    except:
        return None

while not get_counting_element():
    print("Click the first photo to continue")
    time.sleep(0.2)

while True:
    el = get_counting_element()

    # Read current and total number of images
    idx, total = map(int, re.match(
        '([0-9]*) af ([0-9]*)', el.text).groups())
    url = driver.execute_script(_DOWNLOADIMAGE_JS)
    comments = driver.find_element_by_id(
        'controlsgalleryNew_RadRotator2_i0_divComments'
    ).text
    taken = driver.find_element_by_id(
        'controlsgalleryNew_RadRotator2_i0_divDateTaken'
    ).text

    fn = os.path.basename(urlparse(url).path)
    print("Getting image {} of {}, from \"{}\"; {}".format(
        idx, total, taken, fn))

    with TemporaryFile() as tmp:
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            for chunk in r:
                tmp.write(chunk)
        else:
            raise requests.exceptions.HTTPError(
                "Status code was {} for {}".format(r.status_code, r.url))

        # Image age from HTTP header
        t = datetime.strptime(
            r.headers['Last-Modified'],
            "%a, %d %b %Y %H:%M:%S GMT",
        )

        # use path with year/month
        fn = os.path.join('data', t.strftime('%Y'), t.strftime('%m'), fn)
        Path(fn).parent.mkdir(parents=True, exist_ok=True)

        # copy over tmp file
        tmp.seek(0)
        with open(fn, 'wb') as f:
            while chunk := tmp.read(1024):
                f.write(chunk)

        # set file modification time from HTTP header
        os.utime(fn, (
            time.mktime(time.localtime()),
            time.mktime(t.timetuple()),
        ))

    print("saving texts")
    with open(os.path.splitext(fn)[0] + '.comments.txt', 'w') as f:
        f.write(comments)
    with open(os.path.splitext(fn)[0] + '.taken.txt', 'w') as f:
        f.write(taken)

    if idx >= total:
        print("Done fetching {} images and text".format(total))
        break

    # Next images
    driver.find_element_by_id("img_right").click()
    time.sleep(0.5)

print("Exiting..")
